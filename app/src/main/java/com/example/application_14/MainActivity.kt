package com.example.application_14

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_first.setOnClickListener {
            val animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.scale)
            tv_first.startAnimation(animation)
        }
        btn_second.setOnClickListener {
            val animation = AnimationUtils.loadAnimation(this,R.anim.rotate)
            tv_second.startAnimation(animation)
        }
        btn_thrd.setOnClickListener {
            val animation = AnimationUtils.loadAnimation(this,R.anim.translate)
            tv_thrd.startAnimation(animation)
        }
        btn_fourth.setOnClickListener {
            val animation = AnimationUtils.loadAnimation(this,R.anim.alpha)
            tv_fourth.startAnimation(animation)
        }
    }
}
